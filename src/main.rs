use bytesize::ByteSize;
use libc::{c_char, statvfs64, strerror_r};
use procinfo::pid::mountinfo_self;
use ascii_table::{Align, AsciiTable, Column};

use std::{array::IntoIter, ffi::CString, fs, mem::MaybeUninit};
use std::os::unix::ffi::OsStrExt;

mod flags;

const IGNORED_FS: &'static [&'static str] = &["proc", "sysfs", "securityfs", "bpf", "cgroup", "cgroup2", "fusectl", "efivarfs", "debugfs"];
const IGNORED_FS2: &'static [(&'static str, &'static str)] = &[("fuse", "portal")];

fn main() {
	let flags = flags::Df::from_env();

	let mut rows = Vec::new();
	let mut devices = Vec::new();
	for mount in mountinfo_self().unwrap() {
		if IGNORED_FS.contains(&&*mount.fs_type.0) {
			continue;
		}
		if let Some(second) = mount.fs_type.1.as_ref() {
			if IGNORED_FS2.contains(&(&mount.fs_type.0, second)) {
				continue;
			}
		}
		// skip multiple FS on the same partition
		let device = (mount.major, mount.minor);
		if devices.contains(&device) {
			continue;
		}
		devices.push(device);
		let mut data = MaybeUninit::<statvfs64>::uninit();
		let path = CString::new(mount.mount_point.as_os_str().as_bytes()).unwrap();
		let mut res = unsafe { statvfs64(path.as_ptr(), data.as_mut_ptr()) };
		if res != 0 {
			if res < 0 {
				res = unsafe { *libc::__errno_location() };
			}
			let mut error_buffer = vec![0 as c_char; 256];
			let error: String = unsafe {
				let result = strerror_r(res, error_buffer.as_mut_ptr(), error_buffer.len());
				if result != 0 {
					eprintln!("fail {}", result);
					res.to_string().into()
				} else {
					error_buffer.into_iter().take_while(|&x| x != 0).map(|x| x as u8 as char).collect()
				}
			};
			eprintln!("df: {}: {}", mount.mount_point.display(), error);
			continue;
		}
		let data = unsafe { data.assume_init() };
		if data.f_blocks == 0 {
			continue; // metadata is useless
		}

		let typ = match mount.mount_src.as_deref() {
			Some(path) if path.starts_with('/') => {
				fs::canonicalize(path).unwrap().display().to_string()
			},
			_ => mount.fs_type.0
		};
		rows.push((typ, data.f_blocks, data.f_bfree, mount.mount_point, data.f_frsize));
	}
	let mut ascii_table = AsciiTable::default();
	ascii_table.draw_lines = false;

	let human_readable = flags.as_ref().map(|x| x.human_readable).unwrap_or(false);

	for (i, &(header, align)) in [
		("Filesystem", Align::Left),
		(if !human_readable { "Blocks" } else { "Size" }, Align::Right),
		("Used", Align::Right),
		("Free", Align::Right),
		("Use%", Align::Right),
		("Mounted on", Align::Left)
	].iter().enumerate() {
		let mut column = Column::default();
		column.header = header.to_owned();
		column.align = align;
		ascii_table.columns.insert(i, column);
	}

	ascii_table.print(rows.into_iter().map(
		|(typ, blocks, free, path, frsize)| IntoIter::new(
			[
				typ,
				if human_readable {
					let bytes = blocks * frsize;
					ByteSize(bytes).to_string_as(true)
				} else {
					blocks.to_string()
				},
				if human_readable {
					let bytes = (blocks-free) * frsize;
					ByteSize(bytes).to_string_as(true)
				} else {
					(blocks-free).to_string()
				},
				if human_readable {
					let bytes = free * frsize;
					ByteSize(bytes).to_string_as(true)
				} else {
					free.to_string()
				},
				format!("{:.0}%", ((blocks-free) as f64 * 100.0 / blocks as f64).ceil()),
				path.display().to_string()
			]
		)
	));
}
